# Test APP

## Installation

- cp ./backend/.env.example ./backend/.env
- chmod -R 777 ./backend/storage
- cd ./docker
- docker-compose up -d
- docker-compose exec php composer install
- docker-compose exec php php artisan passport:keys
- docker-compose exec php php artisan migrate:fresh --seed
- cd ../backend 
- npm install
- npm run production


## Testing

Open `http://localhost:8000`

Credentials:
- User: test@test.test
- Password: test