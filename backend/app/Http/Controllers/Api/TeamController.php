<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UpdateTeam;
use App\Http\Resources\Player as PlayerResource;
use App\Team;
use App\Http\Controllers\Controller;
use \App\Http\Resources\Team as TeamResource;

class TeamController extends Controller
{
    public function index()
    {
        $teams = Team::get();
        return TeamResource::collection($teams);
    }

    public function show(Team $team)
    {
        return new TeamResource($team);
    }


    public function store(UpdateTeam $request)
    {
        $team = new Team();

        return $this->update($team, $request);
    }

    public function update(Team $team, UpdateTeam $request)
    {
        $team->fill($request->all());
        $team->save();

        return new TeamResource($team);
    }

    public function destroy(Team $team)
    {
        $team->delete();
    }
}
