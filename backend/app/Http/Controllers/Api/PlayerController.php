<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\UpdatePlayer;
use App\Player;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Http\Resources\Player as PlayerResource;

class PlayerController extends Controller
{
    public function index()
    {
        $players = Player::get();
        return PlayerResource::collection($players);
    }

    public function show(Player $player)
    {
        return new PlayerResource($player);
    }

    public function store(UpdatePlayer $request)
    {
        $player = new Player();

        return $this->update($player, $request);
    }

    public function update(Player $player, Request $request)
    {
        $player->fill($request->all());
        $player->save();

        return new PlayerResource($player);
    }

    public function destroy(Player $player)
    {
        $player->delete();
    }
}
