<?php

namespace App\Http\Controllers\Api;

use App\Team;
use App\Http\Controllers\Controller;
use \App\Http\Resources\Player as PlayerResource;

class TeamPlayersController extends Controller
{
    public function index(Team $team)
    {
        return PlayerResource::collection($team->players()->get());
    }
}
