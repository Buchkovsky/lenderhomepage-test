<?php

use Illuminate\Database\Seeder;
use Laravel\Passport\ClientRepository;

class ClientTokenSeeder extends Seeder
{
    public function run()
    {
        $repository = new ClientRepository();

        $client = $repository->find(2);
        if (!empty($client)) {
            $client->delete();
        }

        $client = $repository->create(null, 'test', env('APP_URL') . '/callback', false, true);
        $client->id = 2;
        $client->secret = 'les9QWupl9pEgaANeARyjVjwpWWnggdQbqo5VYek';
        $client->password_client = true;
        $client->save();
    }
}
