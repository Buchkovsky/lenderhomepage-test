<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class)->create([
            'email' => 'test@test.test',
            'password' => bcrypt('test')
        ]);
        factory(\App\Team::class, 10)->create()
            ->each(function (\App\Team $team) {
                factory(\App\Player::class, 10)->create([
                    'team_id' => $team->id
                ]);
            });

        $this->call(ClientTokenSeeder::class);
    }
}
