import Cookies from 'js-cookie'
import axios from 'axios'

const state = {
    access_token: Cookies.get('access_token') || '',
    status: '',
    hasLoadedOnce: false,
};

const getters = {
    isAuthenticated: state => !!state.access_token,
};

const actions = {
    setAuthToken: ({commit}, payload) => {
        commit('setAuthToken', payload);
    },
};

const mutations = {
    setAuthToken: (state, access_token) => {
        state.access_token = access_token;

        Cookies.set('access_token', access_token);

        axios.defaults.headers.common = {'Authorization': `Bearer ${access_token}`}
    },
};

export default {
    state,
    getters,
    actions,
    mutations,
}