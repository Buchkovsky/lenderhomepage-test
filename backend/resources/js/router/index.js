import Vue from 'vue';
import VueRouter from 'vue-router'

Vue.use(VueRouter);

import Index from '../layouts/index';
import Login from '../layouts/login';
import Dashboard from '../layouts/dashboard';

import PlayersEdit from '../layouts/player/players-edit';
import PlayersList from '../layouts/player/players-list';

import TeamsEdit from '../layouts/teams/teams-edit';
import TeamsList from '../layouts/teams/teams-list';

import {store} from '../store'

Vue.use(VueRouter);

const ifNotAuthenticated = (to, from, next) => {
    if (!store.getters.isAuthenticated) {
        next();
        return
    }
    next('/dashboard')
};

const ifAuthenticated = (to, from, next) => {
    if (store.getters.isAuthenticated) {
        next();
        return
    }
    next('/')
};

const routes = [
    {
        path: '/',
        name: 'login',
        component: Login,
        beforeEnter: ifNotAuthenticated,
    },
    {
        path: '/',
        name: 'layout',
        component: Index,
        beforeEnter: ifAuthenticated,
        children: [
            {
                path: 'dashboard',
                name: 'dashboard',
                component: Dashboard
            },
            {
                path: 'players',
                name: 'playersList',
                component: PlayersList
            },
            {
                path: 'players/:id/edit',
                name: 'playersEdit',
                component: PlayersEdit
            },
            {
                path: 'players/:id',
                name: 'playersNew',
                component: PlayersEdit
            },
            {
                path: 'teams',
                name: 'teamsList',
                component: TeamsList
            },
            {
                path: 'teams/:id/edit',
                name: 'teamsEdit',
                component: TeamsEdit
            },
            {
                path: 'teams/:id',
                name: 'teamsNew',
                component: TeamsEdit
            },
        ]
    },
];

export const router = new VueRouter({
    mode: 'history',
    routes
});