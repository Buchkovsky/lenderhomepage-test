import Vue from 'vue';
import App from './layouts/app';

import ElementUI from 'element-ui';
import locale from 'element-ui/lib/locale/lang/en'

import Cookies from 'js-cookie'

import 'element-ui/lib/theme-chalk/index.css';
import { router } from './router/index';

import { store } from './store';
import axios from "axios/index";
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);
Vue.use(ElementUI, {locale});

if (Cookies.get('access_token')) {
    axios.defaults.headers.common = {'Authorization': `Bearer ${Cookies.get('access_token')}`}
}

const index = new Vue({
    el: '#app',
    components: {App},
    router,
    store,
});

export default index;