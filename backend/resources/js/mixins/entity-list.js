import axios from 'axios';
import { Loading } from 'element-ui';

export default {
    data() {
        return {
            tableData: []
        }
    },

    methods: {
        loadData() {
            let loadingInstance = Loading.service();

            axios.get(`${process.env.MIX_API_URL}/api/${this.entity}`).then((response) => {
                this.tableData = response.data.data.map((r) => {
                    return this.dataMap(r);
                });
                loadingInstance.close();
            });
        },


        deleteItem(data) {
            this.$confirm(`Are you sure that you want to delete entity '${data.name}'?`).then(() => {
                axios.delete(`${process.env.MIX_API_URL}/api/${this.entity}/${data.id}`).then(() => {
                    this.$notify({
                        title: 'Operation performed',
                        message: `Entity '${data.name} deleted`,
                        type: 'success'
                    });

                    this.loadData();
                });
            });
        }
    },

    created() {
        this.loadData();
    }
}