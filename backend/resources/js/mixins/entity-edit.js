import axios from 'axios';
import { Loading } from 'element-ui';

export default {
    computed: {
    },
    methods: {
        loadData() {
            if (this.$route.params.id) {
                this.model.id = this.$route.params.id;

                let loadingInstance = Loading.service();
                let url = `${process.env.MIX_API_URL}/api/${this.entity}/${this.model.id}`;
                axios.get(url).then((response) => {
                    this.model = response.data.data;
                }, () => {
                    this.$notify({
                        title: 'Error',
                        message: `An error occurred`,
                        type: 'error'
                    });
                }).finally(() => {
                    loadingInstance.close();
                })
            }
        },

        save() {
            this.$validator.validate().then(result => {
                if (result) {
                    let loadingInstance = Loading.service();

                    let url = `${process.env.MIX_API_URL}/api/${this.entity}/${this.model.id}`;
                    let method = 'patch';
                    let data= {...this.model};

                    if (this.model.id === null) {
                        url = `${process.env.MIX_API_URL}/api/${this.entity}`;
                        method = 'post';
                        delete data.id;
                    }

                    axios[method](url, data).then((response) => {
                        this.model = response.data.data;

                        this.$notify({
                            title: 'Congratulations',
                            message: `Entity updated`,
                            type: 'success'
                        });
                    }, () => {
                        this.$notify({
                            title: 'Error',
                            message: `An error occurred`,
                            type: 'error'
                        });
                    }).finally(() => {
                        loadingInstance.close();
                    });
                } else {
                    this.$notify({
                        title: 'Warning',
                        message: `Please fix errors first`,
                        type: 'warning'
                    });
                }
            });
        }
    },

    created() {
        this.loadData();
    }
}